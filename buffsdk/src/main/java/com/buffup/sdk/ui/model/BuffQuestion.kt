package com.buffup.sdk.ui.model

import com.buffup.sdk.ui.entity.Answer
import com.buffup.sdk.ui.entity.Author
import com.buffup.sdk.ui.entity.Question

data class BuffQuestion (
    val author: BuffAuthor,
    val question: Question,
    val answers: List<Answer>,
    val timeToShow: Int
)

class BuffAuthor (
    val firstName: String,
    val lastName: String,
    val imageUrl: String
) {
    constructor(author: Author) : this(author.firstName, author.lastName, author.image)

    fun name() = "$firstName $lastName"
}