package com.buffup.sdk.ui.view

import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.widget.FrameLayout
import com.buffup.sdk.R
import com.buffup.sdk.ui.entity.Answer
import com.buffup.sdk.ui.model.BuffQuestion
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.buff_answer.view.*
import kotlinx.android.synthetic.main.buff_question.view.*
import kotlinx.android.synthetic.main.buff_sender.view.*
import kotlinx.android.synthetic.main.view_buff_question.view.*

@SuppressLint("ViewConstructor")
class BuffQuestionView(
    context: Context,
    private val question: BuffQuestion,
    closeClickListener: (() -> Unit)?,
    answerClickListener: ((Answer) -> Boolean)?
) : FrameLayout(context) {

    init {
        inflate(context, R.layout.view_buff_question, this)

        // Improvement option: To use ViewBinding
        Glide.with(this).load(question.author.imageUrl).into(sender_image)
        sender_name.text = question.author.name()

        question_text.text = question.question.title
        question_time_progress.max = question.timeToShow

        question_time.text = "${question.timeToShow}"
        question_time_progress.progress = 0


        question.answers.forEach { answer ->
            val answerView = BuffAnswerView(context, answer)
            answer_container.addView(answerView)
            answerView.answerClickListener = answerClickListener
            answerView.answer_text.text = answer.title
        }

        buff_close.setOnClickListener {
            closeClickListener?.invoke()
        }
    }

    fun onCounterChanged(remainingSeconds: Int?) {
        remainingSeconds?.let { s ->
            question_time.text = "$s"
            question_time_progress.progress = question.timeToShow - s

        } ?: run {
            ObjectAnimator.ofFloat(layout_question_time_progress, "alpha", 0f).apply {
                duration = 200
                start()
            }
        }
    }
}