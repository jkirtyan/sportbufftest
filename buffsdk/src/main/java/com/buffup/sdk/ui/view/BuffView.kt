package com.buffup.sdk.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.FrameLayout
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.buffup.sdk.R
import com.buffup.sdk.ui.model.BuffQuestion
import com.buffup.sdk.ui.viewmodel.BuffViewModel
import java.lang.ClassCastException

class BuffView : FrameLayout {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    private lateinit var activity: FragmentActivity
    private lateinit var viewModel: BuffViewModel
    private var buffQuestionView: BuffQuestionView? = null


    init {
        (context as? FragmentActivity)?.let {
            activity = it
            viewModel = ViewModelProviders.of(it).get(BuffViewModel::class.java)
            it.lifecycle.addObserver(viewModel)
        }
            ?: throw ClassCastException("Please ensure that provided Context is a valid FragmentActivity")

    }


    private val questionToShowObserver = Observer<BuffQuestion> { buffQuestion ->
            buffQuestion?.let {
                showQuestion(it)
            } ?: hideQuestion()
        }

    private val counterObserver = Observer<Int> { remainingSeconds ->
            buffQuestionView?.onCounterChanged(remainingSeconds)
        }


    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        viewModel.onAttached()

        viewModel.questionToShow.observeForever(questionToShowObserver)
        viewModel.counter.observeForever(counterObserver)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        viewModel.onDetached()

        viewModel.questionToShow.removeObserver(questionToShowObserver)
        viewModel.counter.removeObserver(counterObserver)
    }

    private fun showQuestion(question: BuffQuestion) {
        BuffQuestionView(
            context,
            question,
            viewModel::onQuestionCloseButtonClicked,
            viewModel::onAnswerSelected
        ).let { questionView ->
            buffQuestionView = questionView

            questionView.layoutParams =
                LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, Gravity.BOTTOM)
            addView(questionView)

            questionView.startAnimation(
                AnimationUtils.loadAnimation(
                    context,
                    R.anim.entry_anim
                )
            )
        }
    }

    private fun hideQuestion() {
        val anim = AnimationUtils.loadAnimation(context, R.anim.exit_anim)
        anim.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(p0: Animation?) {}

            override fun onAnimationEnd(p0: Animation?) {
                buffQuestionView?.let { questionView ->
                    removeView(questionView)
                }
            }

            override fun onAnimationRepeat(p0: Animation?) {}
        })
        buffQuestionView?.startAnimation(anim)
    }
}