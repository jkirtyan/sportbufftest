package com.buffup.sdk.ui

import android.os.Handler
import android.os.Looper
import android.os.Message
import android.os.SystemClock
import java.util.*
import kotlin.concurrent.thread

class BuffTimer(tickSeconds: Int = 30, periodic: Boolean = true) {
    private var t: Thread
    private var isStarted = false

    private var callback: (() -> Unit)? = null

    private val handler: Handler = object : Handler(Looper.getMainLooper()) {
        override fun handleMessage(msg: Message) {
            if (msg.what == 678) {
                callback?.invoke()
            } else {
                super.handleMessage(msg)
            }
        }
    }

    init {
        t = thread (start = false) {
            isStarted = true
            do {
                var seconds = 0
                while (seconds < tickSeconds) {
                    seconds += 1
                    SystemClock.sleep(1000)
                }

                handler.obtainMessage(678).let {
                    handler.sendMessage(it)
                }
            } while(isStarted && periodic)
            isStarted = false
        }
    }

    fun start(callback: () -> Unit) {
        this.callback = callback

        if (!isStarted) {
            t.start()
        }
    }

    fun stop() {
        isStarted = false
    }
}