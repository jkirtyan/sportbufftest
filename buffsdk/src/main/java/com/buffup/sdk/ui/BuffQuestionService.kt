package com.buffup.sdk.ui

import com.buffup.sdk.ui.entity.QuestionResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface BuffQuestionService {
    @GET("/buffs/{buffId}")
    fun getQuestion(@Path("buffId") buffId: Int): Call<QuestionResponse>
}