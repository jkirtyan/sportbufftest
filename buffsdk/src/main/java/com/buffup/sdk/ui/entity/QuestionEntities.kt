package com.buffup.sdk.ui.entity

import com.google.gson.annotations.SerializedName
import java.util.*

data class QuestionResponse(
    val result: Result
)

data class Result(
    val id: Int,
    @SerializedName("client_id")
    val clientId: Int,
    @SerializedName("stream_id")
    val streamId: Int,
    @SerializedName("time_to_show")
    val timeToShow: Int,
    val priority: Int,
    @SerializedName("created_at")
    val createdAt: Date,
    val author: Author,
    val question: Question,
    val answers: List<Answer>,
    val language: String
)

data class Author(
    @SerializedName("first_name")
    val firstName: String,
    @SerializedName("last_name")
    val lastName: String,
    val image: String
)

data class Question(
    val id: Int,
    val title: String,
    val category: Int
)

data class Answer(
    val id: Int,
    @SerializedName("buff_id")
    val buffId: Int,
    val title: String,
    val image: Map<String, Image>
)

data class Image(
    val id: String,
    val key: String,
    val url: String
)