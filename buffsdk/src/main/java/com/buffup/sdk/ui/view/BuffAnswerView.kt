package com.buffup.sdk.ui.view

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.transition.AutoTransition
import android.transition.TransitionManager
import android.widget.RelativeLayout
import com.buffup.sdk.R
import com.buffup.sdk.ui.entity.Answer
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.buff_answer.view.*


@SuppressLint("ViewConstructor")
class BuffAnswerView(context: Context, answer: Answer): RelativeLayout(context) {

    var answerClickListener: ((Answer) -> Boolean)? = null

    init {
        inflate(context, R.layout.buff_answer, this)

        answer_text.text = answer.title

        Glide.with(this)
            .load(answer.image.values.first().url)
            .error(R.drawable.ic_generic_answer)
            .into(answer_image)

        setOnClickListener {
            if (answerClickListener?.invoke(answer) == true) {
                animateToSelected()
            }
        }
    }

    private fun animateToSelected() {
        val animationDuration = 300L

        val lpImage = answer_image.layoutParams as LayoutParams
        val lpText = answer_text.layoutParams as LayoutParams
        val lpSBg = answer_selected_bg.layoutParams as LayoutParams

        lpSBg.removeRule(START_OF)
        lpImage.addRule(ALIGN_PARENT_END, TRUE)
        lpText.removeRule(END_OF)
        lpText.addRule(START_OF, R.id.answer_text)

        val transition = AutoTransition()
        transition.duration = animationDuration
        TransitionManager.beginDelayedTransition(this, transition)

        answer_image.layoutParams = lpImage
        answer_text.layoutParams = lpText

        answer_selected_bg.alpha = 1f
        answer_text.setTextColor(Color.WHITE)
    }
}