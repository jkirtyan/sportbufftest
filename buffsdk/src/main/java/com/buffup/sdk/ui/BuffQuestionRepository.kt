package com.buffup.sdk.ui

import com.buffup.sdk.ui.model.BuffAuthor
import com.buffup.sdk.ui.model.BuffQuestion
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import kotlin.random.Random

class BuffQuestionRepository {
    private val client = OkHttpClient.Builder().build()
    private val service: BuffQuestionService

    init {
        service = Retrofit.Builder()
            .baseUrl("https://demo2373134.mockable.io")
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
            .create(BuffQuestionService::class.java)
    }

    fun getQuestion(completion: (BuffQuestion?) -> Unit) {
        runRequestWithStatusCode(service.getQuestion(Random.nextInt(1, 6))) { _, success, response ->
            if (success) {
                response?.let {
                    completion(BuffQuestion(
                        BuffAuthor(it.result.author),
                        it.result.question,
                        it.result.answers,
                        it.result.timeToShow
                    ))
                }
            } else {
                completion(null)
            }
        }
    }

    private fun <ResponseType> runRequestWithStatusCode(
        call: Call<ResponseType>,
        completion: (Int, Boolean, ResponseType?) -> Unit
    ) {
        call.enqueue(object : Callback<ResponseType> {
            override fun onFailure(call: Call<ResponseType>, t: Throwable) {
                completion(-1, false, null)
            }

            override fun onResponse(call: Call<ResponseType>, response: Response<ResponseType>) {
                if (response.isSuccessful) {
                    completion(response.code(), true, response.body())
                } else {
                    completion(response.code(), false, null)
                }
            }
        })
    }
}