package com.buffup.sdk.ui.viewmodel

import androidx.lifecycle.*
import com.buffup.sdk.ui.BuffTimer
import com.buffup.sdk.ui.BuffQuestionRepository
import com.buffup.sdk.ui.entity.Answer
import com.buffup.sdk.ui.model.BuffQuestion

class BuffViewModel : ViewModel(), LifecycleObserver {
    private val repository = BuffQuestionRepository()
    private val timer = BuffTimer()


    val counter = MutableLiveData<Int>()
    val questionToShow = MutableLiveData<BuffQuestion>()
    var answer: Answer? = null


    private val onTick = {
        repository.getQuestion {
            counter.postValue(it?.timeToShow)
            questionToShow.postValue(it)

            if (it != null) {
                startCounter()
            }
        }
    }

    private fun startCounter() {
        BuffTimer(1).apply {
            start {
                counter.value?.let { counterValue ->
                    counter.value = counterValue - 1

                    if (counter.value == 0) {
                        questionToShow.value = null
                        stop()
                    }
                } ?: stop()
            }
        }
    }

    private fun stopCounter() {
        counter.postValue(null)
    }

    fun onAttached() {
        timer.start(onTick)
    }

    fun onDetached() {
        timer.stop()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onStop() {
        timer.stop()
    }

    fun onQuestionCloseButtonClicked() {
        stopCounter()
        clearData()
    }

    private fun clearData() {
        questionToShow.postValue(null)
        answer = null
    }

    fun onAnswerSelected(answer: Answer): Boolean {
        if (this.answer == null) {
            questionToShow.value?.let {
                this.answer = answer
                stopCounter()
                BuffTimer(2, false).start {
                    clearData()
                }
                return true
            }

        }
        return false
    }
}