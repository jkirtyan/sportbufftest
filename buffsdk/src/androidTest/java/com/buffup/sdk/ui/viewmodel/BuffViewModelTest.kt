package com.buffup.sdk.ui.viewmodel

import com.buffup.sdk.ui.entity.Answer
import com.buffup.sdk.ui.entity.Image
import com.buffup.sdk.ui.entity.Question
import com.buffup.sdk.ui.model.BuffAuthor
import com.buffup.sdk.ui.model.BuffQuestion
import org.junit.After
import org.junit.Before
import org.junit.Test

import org.junit.Assert.*
import java.util.HashMap

class BuffViewModelTest {

    private lateinit var viewModel: BuffViewModel

    @Before
    fun setUp() {
        viewModel = BuffViewModel()
    }

    @After
    fun tearDown() {

    }

    private fun createFakeAnswer() = Answer(0, 0, "", HashMap())

    @Test
    fun onCloseButtonClicked_hasAnswerBefore_answerChangedToNull() {
        viewModel.answer = createFakeAnswer()
        viewModel.onQuestionCloseButtonClicked()
        assertNull(viewModel.answer)
    }

    @Test
    fun onCloseButtonClicked_hasQuestionBefore_questionChangedToNull() {
        val author = BuffAuthor("", "", "")
        val question = Question(0, "", 0)
        val buffQuestion = BuffQuestion(author, question, listOf(createFakeAnswer()), 0)

        viewModel.questionToShow.postValue(buffQuestion)
        viewModel.onQuestionCloseButtonClicked()
        assertNull(viewModel.questionToShow.value)
    }

    @Test
    fun onAnswerSelected_passAnswer_returnTrue() {
        assert(viewModel.onAnswerSelected(createFakeAnswer()))
    }

    @Test
    fun onAnswerSelected_passAnswerTwice_returnFalse() {
        viewModel.onAnswerSelected(createFakeAnswer())
        assertFalse(viewModel.onAnswerSelected(createFakeAnswer()))
    }
}