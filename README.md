## SportBuff Test

**This project implemented ofr SportBuff to check my skills.**

To implement the requested sdk I worked on it a little bit more than five hours. An additional half was the test writing.

In my solution I used the following libraries:

 - [Retrofit](https://square.github.io/retrofit/) ([LICENSE](http://www.apache.org/licenses/LICENSE-2.0))
 - [Gson](https://github.com/google/gson) ([LICENSE](https://github.com/google/gson/blob/master/LICENSE))
 - [Glide](https://github.com/bumptech/glide) ([LICENSE](https://github.com/bumptech/glide/blob/master/LICENSE))
 
In the SDK I followed the MVVM design pattern and I used some Jetpack components (LiveData, Lifecycle).
The whole business logic is in the BuffViewModel. BuffRepository provides the "remote" data for the viewModel. The viewModel communicates to the BuffView to show or hide the view components. I used LiveData to implement this communication. The viewModel receive all of user inputs.

---

## How to use

It is so easy to use the SDK. You only have to add BuffView to your layout as the root of the view you want to Buff view show on.

**For Example**

In this example the Buff view will shown above the VideoView. BuffView inherited from FrameLayout therefore if you want to set aligment for your view you have to use gravity attribute.

```xml
<com.buffup.sdk.ui.view.BuffView
 	android:id="@+id/buff_view"
	android:layout_width="match_parent"
	android:layout_height="match_parent">
	<VideoView
		android:id="@+id/video"
		android:layout_width="match_parent"
		android:layout_height="match_parent"
		android:gravity="center"
		android:keepScreenOn="true" />
</com.buffup.sdk.ui.view.BuffView>
```

---

## Tests

There are two "big" part of the logic in the SDK.
 - When to show/hide the Buff view (In my case Buff view has to be shown if the questionToShow's value is not null)
 - What to do when user interaction happens
 
- I have tested the onCloseButtonClicked and the onAnswerSelected methods. To test it these called on the right UI element click we need UI tests.
- I did not write tests for network calls because I trust in the REST API. The endpoints have to be tested on backend side.
- I did not write tests for parsing the data comes from backend because I used a popular lib for that and it's covered with [tests](https://github.com/google/gson/tree/master/gson/src/test/java/com/google/gson)
- I did not write test to create BuffQuestion model from the raw data stored in QuestionResponse because currently this conversion is private and I did not want to make it public only to do some tests. A possible solution would be a gateway for convert the data but at this moment only the test was in focus thus this gateway would be overkill (It's relative I know). 

---

## Improvement options

 - Make more scalable (e.g. Gateway for data conversion)
 - Create ViewModels for each individual Views (Store states in model layer, communicate with rx)
 - UI tests
 - Use DI framework (e.g. Dagger)
 - Use RxKotlin instead of callbacks and listeners (?)
